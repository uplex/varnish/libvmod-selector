# Implementation-dependent statistics for VMOD selector

As documented in [README](README.rst), VMOD selector may optionally
generate statistics that can be viewed with a tool like varnishstat(1)
(if the method `.create_stats()` is invoked in `vcl_init`). These
describe properties of the set object, and many of them refer to
properties of the object's internal data structures, dependent on the
internal implementation. These are described in the following.

Since they depend on the implementation, these stats may change in any
new version of the VMOD. This will be not considered a breaking
change. Consult the version of this document corresponding to your
version of the VMOD for current information about the statistics.

All of the stats values are constant, never changing during the
lifetime of the VCL instance.

As noted in [README](README.rst), the stats as displayed by
Varnish tools have this naming schema:

```
SELECTOR.<vcl>.<object>.<stat>
```

... where `<vcl>` is the VCL instance name, `<object>` is the object
name, and `<stat>` is the statistic. The names documented in the
following appear in the `<stat>` position.

## Hash stats

For the `.match()` operation, the VMOD constructs data structures for
perfect hashing based on universal hashing. For hash lookup, the VMOD
initially computes a primary hash function. For hash buckets at which
strings in the set are known to collide, the VMOD computes a secondary
hash, specific to each such bucket and limited to the colliding
strings.  The secondary hashes never collide.

Stat names concerning the hashes are prefixed with `hash_`:

* `hash_buckets`: the number of buckets in the primary hash table.
  This is the smallest power of two larger than the number of strings
  in the set.

* `hash_collisions`: the number of values in the primary hash to which
  more than one string in the set is hashed

* `hash_keylen`: the length of the primary hash key vector, in blocks
  of 8 bytes

* `hash_h2_buckets_min`: the minimum number of buckets in a secondary
  hash table. Similar to the primary hash, the number of buckets in
  each secondary hash is the smallest power of two larger than the
  number of strings represented by the hash.

* `hash_h2_buckets_max`: the maximum number of buckets in a secondary
  hash table

* `hash_h2_buckets_avg`: the average number of buckets in a secondary
  hash table, rounded to the nearest integer

* `hash_h2_strings_min`: the minimum number of strings in a secondary
  hash table

* `hash_h2_strings_max`: the maximum number of strings in a secondary
  hash table

* `hash_h2_strings_avg`: the average number of strings in a secondary
  hash table, rounded to the nearest integer

* `hash_h2_klen_min`: the minimum key vector length in a secondary
  hash table, in blocks of 8 bytes

* `hash_h2_klen_max`: the maximum key vector length in a secondary
  hash table

* `hash_h2_klen_avg`: the average key vector length in a secondary
  hash table, rounded to the nearest integer

## Trie stats

For the `.hasprefix()` operation, the VMOD constructs a "quadbit
patricia trie" -- a trie for which each character is examined a nibble
at a time, and for which each node may branch up to 16 ways to the
next nodes.

Stat names concerning tries are prefixed with `trie_`:

* `trie_nodes`: the total number of nodes in the trie

* `trie_nodesz`: the size of a trie node in bytes

* `trie_leaves`: the number of leaf nodes in the trie

* `trie_depth_min`: the minimum depth of a terminating node in the
  trie -- a node at which an element of the set may be found. Note
  that a terminating node may be a non-leaf node, if there are strings
  that are prefixes of other strings in the same set.

* `trie_depth_max`: the maximum depth of a terminating node in the
  trie

* `trie_depth_avg`: the average depth of a terminating node in the
  trie, rounded to the nearest integer

* `trie_fanout_min`: the minimum number of branches at a non-leaf trie
  node. Fanout is always in the range 1 to 16.

* `trie_fanout_max`: the maximum number of branches at a non-leaf trie
  node

* `trie_fanout_avg`: the average number of branches at a non-leaf trie
  node, rounded to the nearest integer
