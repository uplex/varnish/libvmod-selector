..
	This is *NOT* a RST file but the syntax has been chosen so
	that it may become an RST file at some later date.

.. varnish_vsc_begin::  selector
	:oneliner:      VMOD selector set object stats
	:order:         100

.. varnish_vsc:: elements
	:type:  gauge
	:level:  debug
	:oneliner:      Elements

	Number of elements (strings) in the set.

.. varnish_vsc:: setsz
	:type:  gauge
	:level:  debug
	:format: bytes
	:oneliner:      Set size

	Total size of the strings in the set in bytes -- the sum of
	the lengths of all elements, including terminating null bytes.

.. varnish_vsc:: minlen
	:type:  gauge
	:level:  debug
	:format: bytes
	:oneliner:      Minimum string length

	Minimum length of a string in the set in bytes, excluding the
	terminating null byte

.. varnish_vsc:: maxlen
	:type:  gauge
	:level:  debug
	:format: bytes
	:oneliner:      Maximum string length

	Maximum length of a string in the set in bytes, excluding the
	terminating null byte

.. varnish_vsc:: hash_buckets
	:type:  gauge
	:level:  debug
	:oneliner:      Hash table buckets

	Number of buckets in the primary hash table

.. varnish_vsc:: hash_collisions
	:type:  gauge
	:level:  debug
	:oneliner:      Hash value collisions

	Number of values in the primary hash to which more than one
	string in the set is hashed.

.. varnish_vsc:: hash_keylen
	:type:  gauge
	:level:  debug
	:oneliner:      Primary hash key vector length

	Length of the primary hash key vector, in blocks of 8 bytes

.. varnish_vsc:: hash_h2_buckets_min
	:type:  gauge
	:level:  debug
	:oneliner:      Minimum secondary hash buckets

	Minimum number of buckets in a secondary hash table

.. varnish_vsc:: hash_h2_buckets_max
	:type:  gauge
	:level:  debug
	:oneliner:      Maximum secondary hash buckets

	Maximum number of buckets in a secondary hash table

.. varnish_vsc:: hash_h2_buckets_avg
	:type:  gauge
	:level:  debug
	:oneliner:      Average secondary hash buckets

	Average number of buckets in a secondary hash table, rounded
	to the nearest integer

.. varnish_vsc:: hash_h2_strings_min
	:type:  gauge
	:level:  debug
	:oneliner:      Minimum secondary hash strings

	Minimum number of strings in a secondary hash table

.. varnish_vsc:: hash_h2_strings_max
	:type:  gauge
	:level:  debug
	:oneliner:      Maximum secondary hash strings

	Maximum number of strings in a secondary hash table

.. varnish_vsc:: hash_h2_strings_avg
	:type:  gauge
	:level:  debug
	:oneliner:      Average secondary hash strings

	Average number of strings in a secondary hash table, rounded
	to the nearest integer

.. varnish_vsc:: hash_h2_klen_min
	:type:  gauge
	:level:  debug
	:oneliner:      Minimum secondary hash key vector length

	Minimum key vector length for a secondary hash

.. varnish_vsc:: hash_h2_klen_max
	:type:  gauge
	:level:  debug
	:oneliner:      Maximum secondary hash key vector length

	Maximum key vector length for a secondary hash

.. varnish_vsc:: hash_h2_klen_avg
	:type:  gauge
	:level:  debug
	:oneliner:      Average secondary hash key vector length

	Average key vector length for secondary hashes, rounded to the
	nearest integer

.. varnish_vsc:: trie_nodes
	:type:  gauge
	:level:  debug
	:oneliner:      Trie nodes

	Total number of nodes in the trie.

.. varnish_vsc:: trie_nodesz
	:type:  gauge
	:level:  debug
	:format: bytes
	:oneliner:      Trie node size

	Size of a trie node in bytes.

.. varnish_vsc:: trie_leaves
	:type:  gauge
	:level:  debug
	:oneliner:      Trie leaf nodes

	Number of leaf nodes in the trie.

.. varnish_vsc:: trie_depth_min
	:type:  gauge
	:level:  debug
	:oneliner:      Minimum terminating trie node depth

	Minimum depth of a node in the trie at which an element of the
	set may be found.

.. varnish_vsc:: trie_depth_max
	:type:  gauge
	:level:  debug
	:oneliner:      Maximum terminating trie node depth

	Maximum depth of a node in the trie at which an element of the
	set may be found.

.. varnish_vsc:: trie_depth_avg
	:type:  gauge
	:level:  debug
	:oneliner:      Average terminating trie node depth

	Average depth of nodes in the trie at which an element of the
	set may be found, rounded to the nearest integer.

.. varnish_vsc:: trie_fanout_min
	:type:  gauge
	:level:  debug
	:oneliner:      Minimum trie node fanout

	Minimum number of branches at a non-leaf trie node

.. varnish_vsc:: trie_fanout_max
	:type:  gauge
	:level:  debug
	:oneliner:      Maximum trie node fanout

	Maximum number of branches at a non-leaf trie node

.. varnish_vsc:: trie_fanout_avg
	:type:  gauge
	:level:  debug
	:oneliner:      Average trie node fanout

	Average number of branches at a non-leaf trie node, rounded to
	the nearest integer

.. varnish_vsc_end::    selector
