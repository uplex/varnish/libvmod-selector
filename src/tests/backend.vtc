# looks like -*- vcl -*-

varnishtest "backend() method"

varnish v1 -vcl {
	import ${vmod_selector};
	import std;

	backend b1 { .host = "${bad_ip}"; }
	backend b2 { .host = "${bad_ip}"; }
	backend b3 { .host = "${bad_ip}"; }
	backend b4 { .host = "${bad_ip}"; }
	backend b5 { .host = "${bad_ip}"; }

	sub vcl_init {
		new s = selector.set();
		s.add("foo", backend=b1);
		s.add("bar", backend=b2);
		s.add("baz", backend=b3);
		s.add("quux", backend=b4);
		s.add("foobar", backend=b5);
	}

	sub vcl_recv {
		return (synth(200));
	}

	sub vcl_synth {
		set resp.http.N-1 = s.backend(1);
		set resp.http.N-2 = s.backend(2);
		set resp.http.N-3 = s.backend(3);
		set resp.http.N-4 = s.backend(4);
		set resp.http.N-5 = s.backend(5);

		if (req.http.N) {
			set resp.http.N = s.backend(std.integer(req.http.N));
		}

		if (s.match(req.http.Word)) {
			set resp.http.Backend = s.backend();
			set resp.http.Backend-Unique = s.backend(select=UNIQUE);
			set resp.http.Backend-Exact = s.backend(select=EXACT);
			set resp.http.Backend-First = s.backend(select=FIRST);
			set resp.http.Backend-Last = s.backend(select=LAST);
			set resp.http.Backend-Shortest
			    = s.backend(select=SHORTEST);
			set resp.http.Backend-Longest
			    = s.backend(select=LONGEST);
		}

		set resp.http.Foo = s.backend(element="foo");
		set resp.http.Bar = s.backend(element="bar");
		set resp.http.Baz = s.backend(element="baz");
		set resp.http.Quux = s.backend(element="quux");
		set resp.http.Foobar = s.backend(element="foobar");
		if (req.http.Element) {
			set resp.http.Element
				= s.backend(element=req.http.Element);
		}

		return (deliver);
	}
} -start

client c1 {
	txreq
	rxresp
	expect resp.status == 200
	expect resp.http.N-1 == "b1"
	expect resp.http.N-2 == "b2"
	expect resp.http.N-3 == "b3"
	expect resp.http.N-4 == "b4"
	expect resp.http.N-5 == "b5"

	expect resp.http.Foo == "b1"
	expect resp.http.Bar == "b2"
	expect resp.http.Baz == "b3"
	expect resp.http.Quux == "b4"
	expect resp.http.Foobar == "b5"

	txreq -hdr "Word: foo"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b1"
	expect resp.http.Backend-Unique == resp.http.Backend
	expect resp.http.Backend-Exact == resp.http.Backend
	expect resp.http.Backend-First == "b1"
	expect resp.http.Backend-Last == "b1"
	expect resp.http.Backend-Shortest == "b1"
	expect resp.http.Backend-Longest == "b1"

	txreq -hdr "Word: bar"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b2"
	expect resp.http.Backend-Unique == resp.http.Backend
	expect resp.http.Backend-Exact == resp.http.Backend
	expect resp.http.Backend-First == "b2"
	expect resp.http.Backend-Last == "b2"
	expect resp.http.Backend-Shortest == "b2"
	expect resp.http.Backend-Longest == "b2"

	txreq -hdr "Word: baz"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b3"
	expect resp.http.Backend-Unique == resp.http.Backend
	expect resp.http.Backend-Exact == resp.http.Backend
	expect resp.http.Backend-First == "b3"
	expect resp.http.Backend-Last == "b3"
	expect resp.http.Backend-Shortest == "b3"
	expect resp.http.Backend-Longest == "b3"

	txreq -hdr "Word: quux"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b4"
	expect resp.http.Backend-Unique == resp.http.Backend
	expect resp.http.Backend-Exact == resp.http.Backend
	expect resp.http.Backend-First == "b4"
	expect resp.http.Backend-Last == "b4"
	expect resp.http.Backend-Shortest == "b4"
	expect resp.http.Backend-Longest == "b4"

	txreq -hdr "Word: foobar"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b5"
	expect resp.http.Backend-Unique == resp.http.Backend
	expect resp.http.Backend-Exact == resp.http.Backend
	expect resp.http.Backend-First == "b5"
	expect resp.http.Backend-Last == "b5"
	expect resp.http.Backend-Shortest == "b5"
	expect resp.http.Backend-Longest == "b5"
} -run

client c1 {
	txreq -hdr "N: -1"
	rxresp
	expect resp.status == 500
} -run

client c1 {
	txreq -hdr "N: 6"
	rxresp
	expect resp.status == 500
} -run

client c1 {
	txreq -hdr "Element: oof"
	rxresp
	expect resp.status == 500
} -run

logexpect l1 -v v1 -d 1 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(\) called without prior match$}
	expect * = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(6\): set has 5 elements$}
	expect * = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(element="oof"\): no such element$}
	expect * = VCL_return fail
	expect * = End
} -run

varnish v1 -vcl {
	import ${vmod_selector};
	backend b1 { .host = "${bad_ip}"; }
	backend b2 { .host = "${bad_ip}"; }
	backend b3 { .host = "${bad_ip}"; }
	backend b4 { .host = "${bad_ip}"; }

	sub vcl_init {
		new s = selector.set();
		s.add("foobarbazquux", backend=b1);
		s.add("foobarbaz", backend=b2);
		s.add("foobar", backend=b3);
		s.add("foo", backend=b4);
	}

	sub vcl_recv {
		return (synth(200));
	}

	sub vcl_synth {
		if (req.http.Select == "UNIQUE" && s.hasprefix(req.http.Word)) {
			set resp.http.Backend-Unique = s.backend(select=UNIQUE);
		}
		elsif (req.http.Select == "EXACT"
		       && s.hasprefix(req.http.Word)) {
			set resp.http.Backend-Exact = s.backend(select=EXACT);
		}
		elsif (s.hasprefix(req.http.Word)) {
			if (s.nmatches() == 1) {
				set resp.http.Backend = s.backend();
				set resp.http.Backend-Unique
					= s.backend(select=UNIQUE);
			}
			if (s.matched(select=EXACT)) {
				set resp.http.Backend-Exact
					= s.backend(select=EXACT);
			}
			set resp.http.Backend-First = s.backend(select=FIRST);
			set resp.http.Backend-Last = s.backend(select=LAST);
			set resp.http.Backend-Shortest
			    = s.backend(select=SHORTEST);
			set resp.http.Backend-Longest
			    = s.backend(select=LONGEST);
		}
		if (req.http.Element) {
			set resp.http.Element
				= s.backend(element=req.http.Element);
			set resp.http.Element-Unique = s.backend(select=UNIQUE);
			set resp.http.Element-Exact = s.backend(select=EXACT);
			set resp.http.Element-First = s.backend(select=FIRST);
			set resp.http.Element-Last = s.backend(select=LAST);
			set resp.http.Element-Shortest
				= s.backend(select=SHORTEST);
			set resp.http.Element-Longest
				= s.backend(select=LONGEST);
		}
		return (deliver);
	}
}

client c1 {
	txreq -hdr "Word: foo"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b4"
	expect resp.http.Backend-Unique == "b4"
	expect resp.http.Backend-Exact == "b4"
	expect resp.http.Backend-First == "b4"
	expect resp.http.Backend-Last == "b4"
	expect resp.http.Backend-Shortest == "b4"
	expect resp.http.Backend-Longest == "b4"

	txreq -hdr "Word: foobar"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == <undef>
	expect resp.http.Backend-Unique == <undef>
	expect resp.http.Backend-Exact == "b3"
	expect resp.http.Backend-First == "b3"
	expect resp.http.Backend-Last == "b4"
	expect resp.http.Backend-Shortest == "b4"
	expect resp.http.Backend-Longest == "b3"

	txreq -hdr "Word: foobarbaz"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == <undef>
	expect resp.http.Backend-Unique == <undef>
	expect resp.http.Backend-Exact == "b2"
	expect resp.http.Backend-First == "b2"
	expect resp.http.Backend-Last == "b4"
	expect resp.http.Backend-Shortest == "b4"
	expect resp.http.Backend-Longest == "b2"

	txreq -hdr "Word: foobarbazquux"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == <undef>
	expect resp.http.Backend-Unique == <undef>
	expect resp.http.Backend-Exact == "b1"
	expect resp.http.Backend-First == "b1"
	expect resp.http.Backend-Last == "b4"
	expect resp.http.Backend-Shortest == "b4"
	expect resp.http.Backend-Longest == "b1"

	txreq -hdr "Word: foobarb"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == <undef>
	expect resp.http.Backend-Unique == <undef>
	expect resp.http.Backend-Exact == <undef>
	expect resp.http.Backend-First == "b3"
	expect resp.http.Backend-Last == "b4"
	expect resp.http.Backend-Shortest == "b4"
	expect resp.http.Backend-Longest == "b3"

	txreq -hdr "Element: foo"
	rxresp
	expect resp.status == 200
	expect resp.http.Element == "b4"
	expect resp.http.Element-Unique == "b4"
	expect resp.http.Element-Exact == "b4"
	expect resp.http.Element-First == "b4"
	expect resp.http.Element-Last == "b4"
	expect resp.http.Element-Shortest == "b4"
	expect resp.http.Element-Longest == "b4"
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(select=UNIQUE\): 2 elements were matched$}
	expect * = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(select=UNIQUE\): 3 elements were matched$}
	expect * = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(select=UNIQUE\): 4 elements were matched$}
	expect * = VCL_return fail
	expect * = End

	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(select=EXACT\): no element matched exactly$}
	expect * = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Word: foobar" -hdr "Select: UNIQUE"
	rxresp
	expect resp.status == 500
} -run

client c1 {
	txreq -hdr "Word: foobarbaz" -hdr "Select: UNIQUE"
	rxresp
	expect resp.status == 500
} -run

client c1 {
	txreq -hdr "Word: foobarbazquux" -hdr "Select: UNIQUE"
	rxresp
	expect resp.status == 500
} -run

client c1 {
	txreq -hdr "Word: foobarb" -hdr "Select: EXACT"
	rxresp
	expect resp.status == 500
} -run

logexpect l1 -wait

varnish v1 -vcl {
	import ${vmod_selector};
	backend b { .host = "${bad_ip}"; }
	backend b0 None;

	sub vcl_init {
		new s = selector.set();
		s.add("foo", backend=b);
		s.add("bar");
		s.add("bazz", backend=b0);
	}

	sub vcl_recv {
		return (synth(200));
	}

	sub vcl_synth {
		if (s.match(req.http.Word)) {
			set resp.http.Backend = s.backend();
		}
		return (deliver);
	}
}

client c1 {
	txreq -hdr "Word: foo"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == "b"

	txreq -hdr "Word: bazz"
	rxresp
	expect resp.status == 200
	expect resp.http.Backend == ""
} -run

logexpect l1 -v v1 -d 0 -g vxid -q "VCL_Error" {
	expect 0 * Begin req
	expect * = VCL_Error {^vmod selector failure: s\.backend\(\): backend not added for element 2$}
	expect * = VCL_return fail
	expect * = End
} -start

client c1 {
	txreq -hdr "Word: bar"
	rxresp
	expect resp.status == 500
} -run

logexpect l1 -wait
